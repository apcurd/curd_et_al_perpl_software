"""
relative_positions.py

Functions for finding relative positions between points in 3D space and
plotting as a relative positions density function.

Will take localisations and save the relative positions between them,
within a filter distance applied in 3D.

Alistair Curd
University of Leeds
30 July 2018

Software Engineering practices applied

Joanna Leng (an EPSRC funded Research Software Engineering Fellow (EP/R025819/1)
University of Leeds
January 2019

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""


import os
import sys
import argparse
import datetime
import timeit
import time
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import numpy as np
import plotting
import utils
import reports



def get_inputs(info):
    """Creates a file browser to read the filename of the input data. Then asks
    for takes other inputs as text from the command line. Puts these inputs
    parameters into the dictionary that is easy to pass to many functions.
    These are:
        in_file_and_path (string):
            The input filename and path.
        dims (int):
            The dimensions of the data ie 2D or 3D.
        filterdist (int):
            The distance within which relative positions were calculated.
        zoom (int):
            Magnification factor for the centre of the data in a scatter plot.
        verbose (Boolean):
            If True prints outputs to screen as program executes.
    Args:
        info (dict):
            A python dictionary containing a collection of useful parameters
            such as the filenames and paths.

    Returns:
        Nothing
    """

    #root = Tk()
    Tk().withdraw()

    print('\n\nPlease select a localisations file. This should be a .csv '
          '(or .txt with comma delimiters) or .npy, containing one '
          'localisation per row.'
          '\nThe first two or three columns, assumed to be X and Y '
          '(and Z, if 3D is chosen later), will be used for analysis.\n')

    in_file = askopenfilename()

    #root.destroy()

    print("The file you selected is: ", in_file, "\n")

    info['in_file_and_path'] = in_file


    print('How many spatial dimensions shall we use (2 or 3)?')

    try:
        data_dims = int(input('These should be the first 2 or 3 columns of '
                              'your input file: '))
    except ValueError:
        print('This must be an integer.\n')
        sys.exit("The dimensions must be an integer.\n")

    if data_dims not in (2, 3):
        print('The dimensions must be the value 2 or 3.\n')
        sys.exit("The dimensions must be 2 or 3.\n")

    print("\n"+str(data_dims)+" dimensions were selected.\n")

    info['dims'] = data_dims

    print('We will identify neighbouring localisations within a set distance '
          '(filter distance). The smaller the distance the quicker the '
          'calculation.')

    try:
        filterdist = int(input('What is the filter distance between '
                               'localisations that you want to apply (nm)? '))
    except ValueError:
        print('This must be an integer.\n')
        sys.exit("The filter distance must be an integer.\n")

    print("\n"+str(filterdist)+" nm filter distance was selected.\n")

    info['filter_dist'] = filterdist

    #print('Scatter plots of the raw data are plotted. A zoom scatter plot of '
    #      'the centre is also plotted.')
    #print('If you need a zoom but not of the centre please use an interactive '
    #      'visualization system.\n')

    #try:
    #    zoom = int(input('What zoom magnification do you want (any non '
    #                     'integer answer sets the value to 10)? '))
    #except ValueError:
    #    print('\nYou did not select a suitable value so this is set to 10.\n')
    #    zoom = 10

    #if zoom < 1:
    #    zoom = 10

    #info['zoom'] = zoom

    print('Do you want updates printed to the screen as the analysis progresses?')


    silent = False
    answer = input('yes/no \n').lower()
    if answer.startswith('y'):
        silent = True

    info['verbose'] = silent





def read_data_in(info):
    """Reads data from the input file thats filename is provided as an argument
       to this program (relative_positions.py) or from the command line while
       this program executes. Also extracts unful substrings from the input
       filename that will be used to outpur results files and puts them in the
       info dictionary. These are:
          results_dir (str): All output files are saved in a directory at the same
                             level in the directory structure as the input data and with the
                             name that consists of the input file and a date stamp.
          in_file_no_path (str): The input file name with no path.
          filename_without_extension (str): Input file name wihtout the path and
                            file extension. It is used to create a unique name of the output
                            data file and directory.

    Args:
        info (dict): A python dictionary containing a collection of useful parameters
            such as the filenames and paths.
    Returns:
               xyz_values (numpy array): A numpy array of the x, y (and z) localisations.
    """

    in_file = info['in_file_and_path']

    if not os.path.exists(in_file):
        sys.exit("ERROR; The input file does not exist.")


    if in_file[-4:] == '.npy':
        try:
            xyz_values = np.load(in_file)
        except (EOFError, IOError, OSError) as exception:
            print("\n\nCould not read file: ", in_file)
            print("\n\n", type(exception))
            sys.exit("Could not read the input file "+in_file+".\n")
    elif in_file[-4:] == '.csv' or in_file[-4:] == '.txt':
        try:
            skip = 0
            line = open(in_file).readline()
            for cell in line.split(','):
                try:
                    float(cell)
                except ValueError:
                    #print("Not a float")
                    skip = 1
            xyz_values = np.loadtxt(in_file, delimiter=',', skiprows=skip)
        except (EOFError, IOError, OSError) as exception:
            print("\n\nCould not read file: ", in_file)
            print("\n\n", type(exception))
            sys.exit("Could not read the input file "+in_file+".\n")
    else:
        xyz_values = 'Ouch'
        print('Sorry, wrong format!\n')
        sys.exit("The input file "+in_file+" has the wrong format.\n")


    info['values'] = xyz_values.shape[0]
    info['columns'] = xyz_values.shape[1]
    info['total_values'] = xyz_values.shape[0]
    info['total_columns'] = xyz_values.shape[1]


    return xyz_values



def getdistances(xyz_values,
                 filterdist,
                 verbose=False,
                 sort_and_halve=True):
    """Store all vectors between points within a chosen distance of each other
    in all three dimensions in a numpy array. Also works for 2D.

    Args:
        xyz (numpy array):
            Numpy array of localisations with shape (N, 2 or 3),
            where N is the number of localisations.
        filterdist (float):
            Distance (in all three dimensions) between points within
            which relative positions are calculated. This can be chosen by
            user input as the function runs, or by specifying when calling
            the function from a script.
        verbose (Boolean):
            Choice whether to print updates to screen. Defaults to False.
        sort_and_halve (Boolean):
            Choice whether to perform sorting and duplicate removal.
            Defaults to True.

    Returns:
        d (numpy array):
            A numpy array of vectors of neighbours within the filter distance
            for every localisation.
    """

    start_time = time.time()  # Start timing it.

    xyz_filter_values = np.array([filterdist, filterdist, filterdist])

    if verbose:
        print('\nFinding vectors to nearby localisations:\n')

    # Add 3rd column to 2D localisations
    if xyz_values.shape[1] == 2:
        xyz_values = np.column_stack((xyz_values, np.zeros(xyz_values.shape[0])))

    # Initialise d (array of relative positions)
    separation_values = []

    # Find relative positions of near neighbours to first point.
    # Keep going through the list of points until at least one near neighbour
    # is found.
    xyz_index = 0

    # finding all the separtions in x, y (and z) for the first locatisation
    while len(separation_values) == 0:
        if xyz_index > len(xyz_values) -1:
            break

        one_xyz_value = xyz_values[xyz_index]  # Reference localisation


        # A filter to find localisation coordinates within filterdist of loc
        # Gives np.array of True / False
        boolean_test_results = np.logical_and(
            xyz_values > one_xyz_value - xyz_filter_values,
            xyz_values < one_xyz_value + xyz_filter_values)


        # Find indices of localisations within filterdist
        # in all three dimensions (all True) and select these
        test_results_indices = np.all(boolean_test_results, axis=1)
        subxyz = xyz_values[test_results_indices]

        sys.stdout.flush()

        # Populate d with any near neighbours.
        # len(subxyz) == 1 would mean only the reference localisation was
        # within filterdist.
        # Remove [0,0,0], these are duplicates and can overwhelm the result.
        # Store the vectors from reference loc to the filtered subset
        # of all locs.
        if len(subxyz) != 1:
            separation_values = subxyz - one_xyz_value
            selectnonzeros = np.any(separation_values != 0, axis=1)
            separation_values = np.compress(selectnonzeros, separation_values, axis=0)


        # Increment reference localisation
        # in search of next localisation with near neighbours.
        xyz_index = xyz_index + 1


    # Continue appending to the array d
    # with vectors to the near-enough neighbours of other localisations.
    for xyz_index in range(xyz_index, len(xyz_values)):
        one_xyz_value = xyz_values[xyz_index]
        boolean_test_results = np.logical_and(xyz_values > one_xyz_value - xyz_filter_values,
                                              xyz_values < one_xyz_value + xyz_filter_values)
        test_results_indices = np.all(boolean_test_results, axis=1)
        subxyz = xyz_values[test_results_indices]
        if len(subxyz) != 1:
            subd = subxyz - one_xyz_value  #  Array of vectors to near-enough neighbours
            selectnonzeros = np.any(subd != 0, axis=1)
            subd = np.compress(selectnonzeros, subd, axis=0)
            separation_values = np.append(separation_values, subd, axis=0)

        # Progress message
        if verbose and xyz_index % 5000 == 0:
            print('Found neighbours for localisation', xyz_index, 'of', repr(len(xyz_values)) +'.')
            print('%i seconds so far.' % (time.time() - start_time))

    if verbose:
        print('Found %i vectors between all localisations' % len(separation_values))
        print('in %i seconds.' % (time.time() - start_time))

    if sort_and_halve is True:
        separation_values = separation_values[separation_values[:, 2].argsort()]
    
        separation_values = separation_values[separation_values[:, 1].argsort(kind='mergesort')]
    
        separation_values = separation_values[separation_values[:, 0].argsort(kind='mergesort')]
    
        #size, cols = separation_values.shape
    
        separation_values = np.array_split(separation_values, 2)

        return separation_values[1]
    
    else:
        return separation_values


def get_vectors(d_values, dims):
    """Calculates the relative positions as vector components. This
    function saves both 2D and 3D data.

    Args:
        d_values: numpy array of localisations with distances between the
                  localisations.
        dim: The dimensions of the data ie 2D or 3D.

    Returns:
        d_values (numpy array): Array of vector components.
    """
    #for i in range(0, 10):
    #    print(d_values[i])
    x_square_values = np.square(d_values[:, 0])
    y_square_values = np.square(d_values[:, 1])

    if dims == 3:
        z_square_values = np.square(d_values[:, 2])

    xy_distance_values = np.sqrt(x_square_values + y_square_values)

    if dims == 3:
        xz_distance_values = np.sqrt(x_square_values + z_square_values)
        yz_distance_values = np.sqrt(y_square_values + z_square_values)
        xyz_distance_values = np.sqrt(x_square_values + y_square_values + z_square_values)


    d_values = np.column_stack((d_values, xy_distance_values))

    if dims == 3:
        d_values = np.column_stack((d_values, xz_distance_values))
        d_values = np.column_stack((d_values, yz_distance_values))
        d_values = np.column_stack((d_values, xyz_distance_values))

    if dims == 2:
        d_values.view('f8,f8,f8,f8').sort(order=['f3'], axis=0)

    if dims == 3:
        d_values.view('f8,f8,f8,f8,f8,f8,f8').sort(order=['f6'], axis=0)


    return d_values


def save_relative_positions(d_values, filterdist, dims, info):
    """Saves the relative positions that have been found in a csv file. This
    function saves both 2D and 3D data.

    Args:
        d_values: numpy array of localisations with distances between the
                  localisations.
        filterdist: distance (in all three dimensions) between points within
            which relative positions are calculated. This can be chosen by user
            input as the function runs, or by specifying when calling the
            function from a script.
        dim: The dimensions of the data ie 2D or 3D.
        info (dict): A python dictionary containing a collection of useful parameters
            such as the filenames and paths.

    Returns:
        outfilename: The path and filename of the output data file. This is
           recorded in the log file.
    """
    out_file_name = info['results_dir']+r'//'+ info['in_file_no_extension'] + \
        '_PERPL-relpos_%.1ffilter.csv' % filterdist

    if dims == 2:
        head = "xx_separation,yy_separation, ,xy_separation"
    elif dims == 3:
        head = ("xx_separation,yy_separation,zz_separation,xy_separation,"
                "xz_separation,yz_separation,xyz_separation")

    try:
        np.savetxt(out_file_name, d_values, delimiter=',', header=head, comments='')
    except (EOFError, IOError, OSError):
        print("Unexpected error:", sys.exc_info()[0])
        sys.exit("Could not create and open the output data file.")

    return out_file_name








def main():
    """Reads input data of point density locations and calculates relative
        poasitions as vectors. Outputs are writen to a file in a directory
        with the name of the inputfile and a time stamp above the directory of
        the input file. The files contains the x, y (and z) localisations.
        If no input argments are provided inputs can be given from the command
        line as it executes.

    Args:
        input_file (FILE): File of localisations which is a .csv (or .txt with
                           comma delimiters) or .npy and containing N
                           localisations in N rows.
        dims (int): The dimensions of the data. This can be 2 or 3.
        filter_dist (int): The filter distance.
        zoom (int): A magnified scatter plot of the centre of the principal
                    view is produced at this level of zoom. Default is 10.
        verbose (Boolean): Increases the output to screen during execution.

    Returns:
        Nothing
    """

    # handle the input arguments (flags)
    prog = 'relative_positions'
    description = 'Calculating the relative positions of points as vectors.'

    info = {'prog':prog,
            'description':description}

    info['start'] = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')


    parser = argparse.ArgumentParser(prog, description)

    parser.add_argument('-i', '--input_file',
                        dest='input_file',
                        type=argparse.FileType('r'),
                        help='File of localisations which is a .csv (or .txt '
                             'with comma delimiters) or .npy and containing N '
                             'localisations in N rows.',
                        metavar="FILE")

    parser.add_argument('-d', '--dims',
                        dest='dims',
                        type=int,
                        default=3,
                        help="Dimensions of the data. It can be 2 or 3.")

    parser.add_argument('-f', '--filter_distance',
                        dest='filter_dist',
                        type=int,
                        default=150,
                        help="Filter distance.")

    parser.add_argument('-z', '--zoom',
                        dest='zoom',
                        type=int,
                        default=10,
                        help='Magnification applied to the scatter plot of the '
                             'principal view of the data.')

    parser.add_argument('-v', '--verbose',
                        help="Increase output verbosity",
                        action="store_true")

    args = parser.parse_args()


    if args.verbose:
        print("Verbosity is turned on.\n")

    if args.dims < 2 or args.dims > 3:
        sys.exit("ERROR; The data can only have 2 or 3 dimensions.")

    info['dims'] = args.dims
    info['filter_dist'] = args.filter_dist


    info['zoom'] = args.zoom
    info['verbose'] = args.verbose


    if args.input_file is None:
        #print("Get the data from the command line as the program executes.")
        get_inputs(info)
    else:
        info['in_file_and_path'] = args.input_file.name

    info['host'], info['ip_address'], info['operating_system'] = utils.find_hostname_and_ip()

    read_start = timeit.default_timer()
    xyz_values = read_data_in(info)
    read_end = timeit.default_timer()
    reading_time = (read_end-read_start)/60

    utils.primary_filename_and_path_setup(info)

    xyz_values = xyz_values[:, 0:info['dims']]

    if info['verbose']:
        print("\nTime to read the input file was: "+str(round(reading_time, 3))+\
              " minutes.\n")
        print('This file contains '+str(info['values'])+' localisations with '
              +str(info['columns'])+' columns per localisation.')

    try:
        os.makedirs(info['results_dir'])
    except OSError:
        print("Unexpected error:", sys.exc_info()[0])
        sys.exit("Could not create directory for the results.")

    plotting.draw_2d_scatter_plots(xyz_values, info['dims'], info, 0)
    plotting.draw_2d_scatter_plots(xyz_values, info['dims'], info, info['zoom'])

    d_values = getdistances(xyz_values, info['filter_dist'], info['verbose'])

    try:
        len(d_values)
    except TypeError:
        print("No data found so we are exiting.")
        sys.exit("No data found so we are exiting.")

    d_values = get_vectors(d_values, info['dims'])


    if info['verbose']:
        print('\nWhen duplicates are removed there are %i vectors within the '
              'filter distance in all dimensions for all localisations.'
              % len(d_values))

    plotting.plot_histograms(d_values, info['dims'], info['filter_dist'], info)

    filter_end = timeit.default_timer()
    filter_time = (filter_end-read_end)/60

    if info['verbose']:
        print("\nTime to filter the data was: "+ str(round(filter_time, 3)) +\
              " minutes.")

    xyz_filename = save_relative_positions(d_values, info['filter_dist'], info['dims'], info)

    save_data_end = timeit.default_timer()
    filtering_time = (save_data_end-filter_end)/60
    if info['verbose']:
        print("\nTime to write the data was: "+str(round(filtering_time, 3))+" minutes.")

    reports.write_rel_pos_html_report(info)

    if info['verbose']:
        print('\nRelative positions are saved in the file:\n' + xyz_filename)











if __name__ == "__main__":
    #Tk().withdraw()
    main()
    #print('\nHit Enter to exit')
    #input()
    