"""
zdisk_modelling.py

Functions to fit models of relative positions in Z-disk data to
relative positions among localisation microscopy data.

Created on Wed Dec 12 14:28:49 2018

Alistair Curd
University of Leeds
30 July 2018

Software Engineering practices applied

Joanna Leng (an EPSRC funded Research Software Engineering Fellow (EP/R025819/1)
University of Leeds
January 2019

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import stats
import linearrepeatmodels as linmods
from modelling_general import ModelWithFitSettings
from modelling_general import stdev_of_model


fitlength = 100.
locprec = 3.4


def getaxialseparations_no_smoothing(relpos,
                                     fitlength=100.,
                                     transverse_limit=10.):
    """Get axial separations between localisations, also within a limited
    transverse distance of one another.

    Args:
        relpos:
            Relative positions containing .axial and .transverse
            distance (e.g. pandas dataframe).
        fitlength:
            The maximum axial separation upto which data will be
            modelled.
        transverselimit:
            The maximum distance across the transverse plane
            between localisations involved.
    Returns:
        sorted_axpoints:
            The axial separations as desired, sorted from lowest to
            highest.
    """
    # Make all axial separations positive.
    relpos.axial = abs(relpos.axial)

    # Select desired data.
    # The smoothing is used like this so that points beyond those not
    # included would only have 1% effect on the values at fitlength
    axpoints = relpos.axial[(relpos.axial < fitlength) &
                            (relpos.transverse < transverse_limit)]
    sorted_axpoints = np.sort(axpoints)
    return sorted_axpoints


def getaxialseparations_with_smoothing(relpos,
                                       fitlength=100.,
                                       locprec=locprec,
                                       transverse_limit=10.,
                                       smoothing=False):
    """Get axial separations between localisations, also within a limited
    transverse distance of one another.

    Args:
        relpos:
            Relative positions containing .axial and .transverse
            distance (e.g. pandas dataframe).
        fitlength:
            The maximum axial separation upto which data will be
            modelled.
        locprec:
            Average localisation precision of the localisations involved.
        transverselimit:
            The maximum distance across the transverse plane
            between localisations involved.
    Returns:
        axpoints:
            The axial separations as desired, sorted from lowest to
            highest.
    """
    # Make all axial separations positive.
    relpos.axial = abs(relpos.axial)

    smoothing = np.sqrt(2) * locprec

    # Select desired data.
    # The smoothing is used like this so that points beyond those not
    # included would only have 1% effect on the values at fitlength
    axpoints = relpos.axial[(relpos.axial < (fitlength + smoothing * 3)) &
                            (relpos.transverse < transverse_limit)]
    sorted_axpoints = np.sort(axpoints)
    return sorted_axpoints


def removeduplicates(sortedrelpos):
    """Slice relative positions so that duplicates are removed. This can be
    necessary when absolute amplitude of the
    histograms/density is important.
    """
    slicedrelpos = sortedrelpos[::2]
    return slicedrelpos


def kde_1nm(distances, locprec=locprec, fitlength=fitlength):
    """Evaluate KDE of distances between localisations at steps of
    1 nm, starting at 0.5 nm. Use localisation precision * np.sqrt(2) as the
    Gaussian kernel.

    Args:
        distances:
            Distances between localisations (e.g. axial separation
                                                    in the Z-disk).
        locprec:
            Average localisation precision of the localisations involved.
        fitlength:
            The distance upto which the KDE will be evaluated.
    Returns:
        x:
            The distances at which the KDE was evaluated.
        distancekde:
            The KDE of the distance distribution, evaluated at x.
    """
    smoothing = np.sqrt(2) * locprec
    kernel_scalar = smoothing / np.std(distances)
    kernel = stats.gaussian_kde(distances, bw_method=kernel_scalar)
    x = np.arange(np.round(smoothing * 3) + 0.5, fitlength, 1.)

    # Multiply to match histogram, as KDE gets normalised as a pdf.
    kde = kernel(x) * len(distances)

    return x, kde


def fitmodel_to_hist(x, experimentaldist,
                     model=linmods.linrepplusreps5fixedpeakratio,
                     initial_params=None,
                     param_bounds=None):
    """Fit model to distance histogram. Designed to allow user editting of
    parameter guesses and bounds for models of RPDs for linear repeating
    models, as in linearrepeatmodels.py.

    Args:
        x:
            Distance values at which the experimental distribution of
            distances and the model are evaluated.
        experimentaldist:
            The values of the experimental distribution of distances. Can be
            e.g. histogram or KDE.
        model:
            The model for the RPD.
        fitlength:
            The maximum distance that may be included in the fit.
    """
    popt, pcov = curve_fit(
            model, x, experimentaldist,
            p0=initial_params,
            bounds=param_bounds
            )
    # plt.plot(x, model(x, *popt))
    perr = np.sqrt(np.diag(pcov))
    params = np.column_stack((popt, perr))
    print(params)
    k = float(len(popt) + 1)  # No. free parameters,
                              # including var. of residuals
                              # for least squares fit.

    ssr = np.sum((model(x, *popt)
                  - experimentaldist) ** 2)
    aic = len(x) * np.log(ssr / len(x)) + 2 * k
    aiccorr = aic + 2 * k * (k + 1) / (len(x) - k - 1)

    print('SSR =', ssr)
    print('AIC =', aic)
    print('AICcorr =', aiccorr)

    return popt, pcov, perr


def plotactnfig(axpoints, popt, sd,
                model=linmods.linrepplusreps5fixedpeakratio,
                color='xkcd:red'):
    plt.figure()

    ax = plt.subplot(111)

    ax.hist(axpoints,
            bins=np.arange(fitlength + 1),
            color='lightblue', alpha=0.5)[0]

    # x, kde = kde_1nm(axpoints)
    # ax.plot(x, kde)

    ax.plot(axpoints, model(axpoints, *popt), color=color, lw=0.75)
    ax.fill_between(axpoints,
                    model(axpoints, *popt) - sd * 1.96,
                    model(axpoints, *popt) + sd * 1.96,
                    facecolor=color, alpha=0.25
                    )

    return ax


def actn_mEos_plot():
    # Get 2D (axial, transverse) relative positions
    relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM/AlexasNearestNeighbours/Actinin/acta2_NNS_aligned_6FOVs_len1229656.pkl')

    # Get subset of axial relative positions
    relpos.axial = abs(relpos.axial)
    fitlength = 100.
    loc_precision = 3.4
    separation_precision = np.sqrt(2) * loc_precision
    # The smoothing is used like this so that points beyond those not
    # included would only have 1.1% effect on the values of the kernal
    # density estimate (KDE) at fitlength.
    axpoints = getaxialseparations_no_smoothing(
            relpos=relpos,
            fitlength=fitlength + 3 * separation_precision,
            transverse_limit=10.
            )

    # Remove duplicates
    axpoints = removeduplicates(axpoints)

    # Find kernel density estimate using localisation precision * sqrt(2)
    # as the Gaussian kernel.
    # Ues mean localisation precision estimate for mEos2:ACTN2,
    # after filtering to < 5 nm.
    kde_x_values, kde = kde_1nm(axpoints,
                                locprec=loc_precision,
                                fitlength=fitlength)

    # Histogram of axial separation, in 1-nm bins
    bin_vals = np.arange(fitlength + 1)
    ax_histogram, bin_values = np.histogram(axpoints,
                                            bins=bin_vals)

    # Centre and width values for histogram bars
    ax_plot_points = (bin_values[:-1] + bin_values[1:]) / 2
    bar_width = 1.

    # Plot histogram and kde for axial separations
    plt.figure()
    axes = plt.subplot(111)
    axes.bar(ax_plot_points,
             ax_histogram,
             align='center',
             width=bar_width,
             color='lightblue', alpha=0.5)
    axes.plot(kde_x_values, kde, lw=0.5, color='blue')

    # Set up models and fit:
    model_with_info = set_up_model_5_variable_peaks_with_fit_settings()

    (params_optimised,
     params_covar,
     params_1sd_error) = fitmodel_to_hist(kde_x_values,
                                          kde,
                                          model_with_info.model_rpd,
                                          model_with_info.initial_params,
                                          model_with_info.param_bounds,
                                          )
    axes.plot(kde_x_values,
              model_with_info.model_rpd(kde_x_values, *params_optimised),
              color='xkcd:red', lw=0.5)

    # Get 1 SD uncertainty on model result from uncertainty on parameters.
    stdev = stdev_of_model(kde_x_values,
                           params_optimised,
                           params_covar,
                           model_with_info.vector_input_model
                           )

    # Plot 95% confidence interval on model
    axes.fill_between(kde_x_values,
                      model_with_info.model_rpd(kde_x_values,
                                                *params_optimised)
                                                - stdev * 1.96,
                      model_with_info.model_rpd(kde_x_values,
                                                *params_optimised)
                                                + stdev * 1.96,
                      color='xkcd:red', alpha=0.25
                      )


def lasp_mEos_plot():
    """Fit axial KDE of relative positions of myopalladin:mEos localisations.
    """
    # Get 2D (axial, transverse) relative positions
    relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM'
                            '/AlexasNearestNeighbours/LASP2'
                            '/nns-aligned-5fovs.pkl')

    # Get subset of axial relative positions
    relpos.axial = abs(relpos.axial)
    fitlength = 100.
    loc_precision = 3.4
    separation_precision = np.sqrt(2) * loc_precision
    # The smoothing is used like this so that points beyond those not
    # included would only have 0.03% effect on the values of the kernal
    # density estimate (KDE) at fitlength.
    axpoints = getaxialseparations_no_smoothing(
            relpos=relpos,
            fitlength=fitlength + 3 * separation_precision,
            transverse_limit=10.
            )

    # Remove duplicates
    axpoints = removeduplicates(axpoints)

    # Find kernel density estimate using localisation precision * sqrt(2)
    # as the Gaussian kernel.
    # Ues mean localisation precision estimate for mEos2:ACTN2,
    # after filtering to < 5 nm.
    kde_x_values, kde = kde_1nm(axpoints,
                                locprec=loc_precision,
                                fitlength=fitlength)

    # Histogram of axial separation, in 1-nm bins
    bin_vals = np.arange(fitlength + 1)
    ax_histogram, bin_values = np.histogram(axpoints,
                                            bins=bin_vals)

    # Centre and width values for histogram bars
    ax_plot_points = (bin_values[:-1] + bin_values[1:]) / 2
    bar_width = 1.

    # Set up models and fit:
    for model_with_info in [# set_up_model_5_variable_peaks_with_fit_settings(),
                            set_up_model_4_variable_peaks_with_fit_settings()
                            ]:
    # model_with_info = set_up_model_5_variable_peaks_with_fit_settings()
    # model_with_info = set_up_model_5_variable_peaks_after_offset_with_fit_settings()
    # model_with_info = set_up_model_linear_fit_with_fit_settings()

        # Plot histogram and kde for axial separations
        plt.figure()
        axes = plt.subplot(111)
        axes.bar(ax_plot_points,
                 ax_histogram,
                 align='center',
                 width=bar_width,
                 color='lightblue', alpha=0.5)
        axes.plot(kde_x_values, kde, lw=0.5, color='blue')

        print('\n'+ model_with_info.model_rpd.__name__)
        print('________________________________')

        (params_optimised,
         params_covar,
         params_1sd_error) = fitmodel_to_hist(kde_x_values,
                                              kde,
                                              model_with_info.model_rpd,
                                              model_with_info.initial_params,
                                              model_with_info.param_bounds,
                                              )
        axes.plot(kde_x_values,
                  model_with_info.model_rpd(kde_x_values, *params_optimised),
                  color='xkcd:red', lw=0.5)

        # Get 1 SD uncertainty on model result from uncertainty on parameters.
        stdev = stdev_of_model(kde_x_values,
                               params_optimised,
                               params_covar,
                               model_with_info.vector_input_model
                               )

        # Plot 95% confidence interval on model
        axes.fill_between(kde_x_values,
                          model_with_info.model_rpd(kde_x_values,
                                                    *params_optimised)
                                                    - stdev * 1.96,
                          model_with_info.model_rpd(kde_x_values,
                                                    *params_optimised)
                                                    + stdev * 1.96,
                          color='xkcd:red', alpha=0.25
                          )

        axes.set_title(model_with_info.model_rpd.__name__)
        axes.set_xlabel('Axial separation (nm)')
        axes.set_ylabel('Counts')


def mypn_mEos_plot():
    """Fit axial KDE of relative positions of myopalladin:mEos localisations.
    """
    # Get 2D (axial, transverse) relative positions
    relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM'
                            '/AlexasNearestNeighbours/Myopalladin'
                            '/MYPN_NNS_aligned_5_FOVs_len1445698.pkl')

    # Get subset of axial relative positions
    relpos.axial = abs(relpos.axial)
    fitlength = 100.
    loc_precision = 3.4
    separation_precision = np.sqrt(2) * loc_precision
    # The smoothing is used like this so that points beyond those not
    # included would only have 0.03% effect on the values of the kernal
    # density estimate (KDE) at fitlength.
    axpoints = getaxialseparations_no_smoothing(
            relpos=relpos,
            fitlength=fitlength + 3 * separation_precision,
            transverse_limit=10.
            )

    # Remove duplicates
    axpoints = removeduplicates(axpoints)

    # Find kernel density estimate using localisation precision * sqrt(2)
    # as the Gaussian kernel.
    # Ues mean localisation precision estimate for mEos2:ACTN2,
    # after filtering to < 5 nm.
    kde_x_values, kde = kde_1nm(axpoints,
                                locprec=loc_precision,
                                fitlength=fitlength)

    # Histogram of axial separation, in 1-nm bins
    bin_vals = np.arange(fitlength + 1)
    ax_histogram, bin_values = np.histogram(axpoints,
                                            bins=bin_vals)

    # Centre and width values for histogram bars
    ax_plot_points = (bin_values[:-1] + bin_values[1:]) / 2
    bar_width = 1.

    # Plot histogram and kde for axial separations
    plt.figure()
    axes = plt.subplot(111)
    axes.bar(ax_plot_points,
             ax_histogram,
             align='center',
             width=bar_width,
             color='lightblue', alpha=0.5)
    axes.plot(kde_x_values, kde, lw=0.5, color='blue')

    # Set up models and fit:
    model_with_info = set_up_model_5_variable_peaks_with_fit_settings()
    # model_with_info = set_up_model_linear_fit_with_fit_settings()

    (params_optimised,
     params_covar,
     params_1sd_error) = fitmodel_to_hist(kde_x_values,
                                          kde,
                                          model_with_info.model_rpd,
                                          model_with_info.initial_params,
                                          model_with_info.param_bounds,
                                          )
    axes.plot(kde_x_values,
              model_with_info.model_rpd(kde_x_values, *params_optimised),
              color='xkcd:red', lw=0.5)

    # Get 1 SD uncertainty on model result from uncertainty on parameters.
    stdev = stdev_of_model(kde_x_values,
                           params_optimised,
                           params_covar,
                           model_with_info.vector_input_model
                           )

    # Plot 95% confidence interval on model
    axes.fill_between(kde_x_values,
                      model_with_info.model_rpd(kde_x_values,
                                                *params_optimised)
                                                - stdev * 1.96,
                      model_with_info.model_rpd(kde_x_values,
                                                *params_optimised)
                                                + stdev * 1.96,
                      color='xkcd:red', alpha=0.25
                      )


def create_default_fitting_params_dicts():
    """Create lists for initial guesses and bounds for parameters during
    fitting. Sets the defaults for scipy.optimise.curve_fit

    Returns:
        lower_bound_dict (dict):
            Dictionary of default lower bound options for parameter values.
        upper_bound_dict (dict):
            Dictionary of default upper bound options for parameter values.
        initial_params_dict (dict):
            Dictionary of default initial parameter value options.
    """
    lower_bound_dict = {
            'repeat_distance': 0,
            'repeat_broadening': 0,
            'first_peak_offset': 0,
            'amp_peak_1': 0,
            'amp_peak_2': 0,
            'amp_peak_3': 0,
            'amp_peak_4': 0,
            'amp_peak_5': 0,
            'amp_peak_6': 0,
            'loc_prec_sd': 0,
            'loc_prec_amp': 0,
            'bg_slope': -100,
            'bg_offset': 0,
            }

    upper_bound_dict = {
            'repeat_distance': 50,
            'repeat_broadening': 20,
            'first_peak_offset': 30,
            'amp_peak_1': 1000,
            'amp_peak_2': 1000,
            'amp_peak_3': 1000,
            'amp_peak_4': 1000,
            'amp_peak_5': 10000,
            'amp_peak_6': 1000,
            'loc_prec_sd': 20,
            'loc_prec_amp': 1000,
            'bg_slope': 100,
            'bg_offset': 100,
            }

    initial_params_dict = {
            'repeat_distance': 20,
            'repeat_broadening': 5,
            'first_peak_offset': 0,
            'amp_peak_1': 1,
            'amp_peak_2': 1,
            'amp_peak_3': 1,
            'amp_peak_4': 1,
            'amp_peak_5': 1,
            'amp_peak_6': 1,
            'loc_prec_sd': 3,
            'loc_prec_amp': 1,
            'bg_slope': -0.2,
            'bg_offset': 20,
            }

    return lower_bound_dict, upper_bound_dict, initial_params_dict


def set_up_model_5_variable_peaks_with_fit_settings():
    """Set up the RPD model with fitting settings.
    The fitting settings are to pass to scipy's
    curve_fit, and the vector-input version of the model is for
    differentiation and error propagation with numdifftools.

    Args:
        None

    Returns:
        A ModelWithFitSettings object containing:
            model_rpd (function name):
                Relative position density as a function of separation
                between localisations.
            initial_params (list):
                Starting guesses for the parameter values by
                scipy.optimize.curve_fit
            lower_bounds (list), upper_bounds (list):
                The bounds on allowable parameter values as
                scipy.optimize.curve_fit runs.
    """
    # Generate ModelWithFitSettings object, conatining a model_rpd
    model_with_fit_settings = (
        ModelWithFitSettings(model_rpd=linmods.linrepnoreps5)
        )

    # Add fitting parameters to ModelWithFitSettings object
    (lower_bound_dict,
     upper_bound_dict,
     initial_params_dict) = create_default_fitting_params_dicts()

    # Can optionally modify these dictionaries here:

    initial_params = [initial_params_dict['repeat_distance'],
                      initial_params_dict['repeat_broadening'],
                      initial_params_dict['amp_peak_1'],
                      initial_params_dict['amp_peak_2'],
                      initial_params_dict['amp_peak_3'],
                      initial_params_dict['amp_peak_4'],
                      initial_params_dict['amp_peak_5'],
                      initial_params_dict['bg_slope'],
                      initial_params_dict['bg_offset']
                      ]

    lower_bounds = [lower_bound_dict['repeat_distance'],
                    lower_bound_dict['repeat_broadening'],
                    lower_bound_dict['amp_peak_1'],
                    lower_bound_dict['amp_peak_2'],
                    lower_bound_dict['amp_peak_3'],
                    lower_bound_dict['amp_peak_4'],
                    lower_bound_dict['amp_peak_5'],
                    lower_bound_dict['bg_slope'],
                    lower_bound_dict['bg_offset']
                    ]

    upper_bounds = [upper_bound_dict['repeat_distance'],
                    upper_bound_dict['repeat_broadening'],
                    upper_bound_dict['amp_peak_1'],
                    upper_bound_dict['amp_peak_2'],
                    upper_bound_dict['amp_peak_3'],
                    upper_bound_dict['amp_peak_4'],
                    upper_bound_dict['amp_peak_5'],
                    upper_bound_dict['bg_slope'],
                    upper_bound_dict['bg_offset']
                    ]

    bounds = (lower_bounds, upper_bounds)

    model_with_fit_settings.initial_params = (
        initial_params
        )
    model_with_fit_settings.param_bounds = (
        bounds
        )
    model_with_fit_settings.vector_input_model = (
        linmods.linrepnoreps5vectorinput
        )

    return model_with_fit_settings


def set_up_model_5_variable_peaks_after_offset_with_fit_settings():
    """Set up the RPD model with fitting settings.
    The fitting settings are to pass to scipy's
    curve_fit, and the vector-input version of the model is for
    differentiation and error propagation with numdifftools.

    Args:
        None

    Returns:
        A ModelWithFitSettings object containing:
            model_rpd (function name):
                Relative position density as a function of separation
                between localisations.
            initial_params (list):
                Starting guesses for the parameter values by
                scipy.optimize.curve_fit
            lower_bounds (list), upper_bounds (list):
                The bounds on allowable parameter values as
                scipy.optimize.curve_fit runs.
    """
    # Generate ModelWithFitSettings object, conatining a model_rpd
    model_with_fit_settings = (
        ModelWithFitSettings(model_rpd=linmods.lin_repeat_after_offset_5)
        )

    # Add fitting parameters to ModelWithFitSettings object
    (lower_bound_dict,
     upper_bound_dict,
     initial_params_dict) = create_default_fitting_params_dicts()

    # Can optionally modify these dictionaries here:

    initial_params = [initial_params_dict['repeat_distance'],
                      initial_params_dict['repeat_broadening'],
                      initial_params_dict['first_peak_offset'],
                      initial_params_dict['amp_peak_1'],
                      initial_params_dict['amp_peak_2'],
                      initial_params_dict['amp_peak_3'],
                      initial_params_dict['amp_peak_4'],
                      initial_params_dict['amp_peak_5'],
                      initial_params_dict['amp_peak_6'],
                      initial_params_dict['bg_slope'],
                      initial_params_dict['bg_offset']
                      ]

    lower_bounds = [lower_bound_dict['repeat_distance'],
                    lower_bound_dict['repeat_broadening'],
                    lower_bound_dict['first_peak_offset'],
                    lower_bound_dict['amp_peak_1'],
                    lower_bound_dict['amp_peak_2'],
                    lower_bound_dict['amp_peak_3'],
                    lower_bound_dict['amp_peak_4'],
                    lower_bound_dict['amp_peak_5'],
                    lower_bound_dict['amp_peak_6'],
                    lower_bound_dict['bg_slope'],
                    lower_bound_dict['bg_offset']
                    ]

    upper_bounds = [upper_bound_dict['repeat_distance'],
                    upper_bound_dict['repeat_broadening'],
                    upper_bound_dict['first_peak_offset'],
                    upper_bound_dict['amp_peak_1'],
                    upper_bound_dict['amp_peak_2'],
                    upper_bound_dict['amp_peak_3'],
                    upper_bound_dict['amp_peak_4'],
                    upper_bound_dict['amp_peak_5'],
                    upper_bound_dict['amp_peak_6'],
                    upper_bound_dict['bg_slope'],
                    upper_bound_dict['bg_offset']
                    ]

    bounds = (lower_bounds, upper_bounds)

    model_with_fit_settings.initial_params = (
        initial_params
        )
    model_with_fit_settings.param_bounds = (
        bounds
        )
    model_with_fit_settings.vector_input_model = (
        linmods.lin_repeat_after_offset_5_vectorargs
        )

    return model_with_fit_settings


def set_up_model_4_variable_peaks_with_fit_settings():
    """Set up the RPD model with fitting settings.
    The fitting settings are to pass to scipy's
    curve_fit, and the vector-input version of the model is for
    differentiation and error propagation with numdifftools.

    Args:
        None

    Returns:
        A ModelWithFitSettings object containing:
            model_rpd (function name):
                Relative position density as a function of separation
                between localisations.
            initial_params (list):
                Starting guesses for the parameter values by
                scipy.optimize.curve_fit
            lower_bounds (list), upper_bounds (list):
                The bounds on allowable parameter values as
                scipy.optimize.curve_fit runs.
    """
    # Generate ModelWithFitSettings object, conatining a model_rpd
    model_with_fit_settings = (
        ModelWithFitSettings(model_rpd=linmods.linrepnoreps4)
        )

    # Add fitting parameters to ModelWithFitSettings object
    (lower_bound_dict,
     upper_bound_dict,
     initial_params_dict) = create_default_fitting_params_dicts()

    # Can optionally modify these dictionaries here:

    initial_params = [initial_params_dict['repeat_distance'],
                      initial_params_dict['repeat_broadening'],
                      initial_params_dict['amp_peak_1'],
                      initial_params_dict['amp_peak_2'],
                      initial_params_dict['amp_peak_3'],
                      initial_params_dict['amp_peak_4'],
                      initial_params_dict['bg_slope'],
                      initial_params_dict['bg_offset']
                      ]

    lower_bounds = [lower_bound_dict['repeat_distance'],
                    lower_bound_dict['repeat_broadening'],
                    lower_bound_dict['amp_peak_1'],
                    lower_bound_dict['amp_peak_2'],
                    lower_bound_dict['amp_peak_3'],
                    lower_bound_dict['amp_peak_4'],
                    lower_bound_dict['bg_slope'],
                    lower_bound_dict['bg_offset']
                    ]

    upper_bounds = [upper_bound_dict['repeat_distance'],
                    upper_bound_dict['repeat_broadening'],
                    upper_bound_dict['amp_peak_1'],
                    upper_bound_dict['amp_peak_2'],
                    upper_bound_dict['amp_peak_3'],
                    upper_bound_dict['amp_peak_4'],
                    upper_bound_dict['bg_slope'],
                    upper_bound_dict['bg_offset']
                    ]

    bounds = (lower_bounds, upper_bounds)

    model_with_fit_settings.initial_params = (
        initial_params
        )
    model_with_fit_settings.param_bounds = (
        bounds
        )
    model_with_fit_settings.vector_input_model = (
        linmods.linrepnoreps4vectorinput
        )

    return model_with_fit_settings


def set_up_model_4_variable_peaks_after_offset_with_fit_settings():
    """Set up the RPD model with fitting settings.
    The fitting settings are to pass to scipy's
    curve_fit, and the vector-input version of the model is for
    differentiation and error propagation with numdifftools.

    Args:
        None

    Returns:
        A ModelWithFitSettings object containing:
            model_rpd (function name):
                Relative position density as a function of separation
                between localisations.
            initial_params (list):
                Starting guesses for the parameter values by
                scipy.optimize.curve_fit
            lower_bounds (list), upper_bounds (list):
                The bounds on allowable parameter values as
                scipy.optimize.curve_fit runs.
    """
    # Generate ModelWithFitSettings object, conatining a model_rpd
    model_with_fit_settings = (
        ModelWithFitSettings(model_rpd=linmods.lin_repeat_after_offset_4)
        )

    # Add fitting parameters to ModelWithFitSettings object
    (lower_bound_dict,
     upper_bound_dict,
     initial_params_dict) = create_default_fitting_params_dicts()

    # Can optionally modify these dictionaries here:

    initial_params = [initial_params_dict['repeat_distance'],
                      initial_params_dict['repeat_broadening'],
                      initial_params_dict['first_peak_offset'],
                      initial_params_dict['amp_peak_1'],
                      initial_params_dict['amp_peak_2'],
                      initial_params_dict['amp_peak_3'],
                      initial_params_dict['amp_peak_4'],
                      initial_params_dict['bg_slope'],
                      initial_params_dict['bg_offset']
                      ]

    lower_bounds = [lower_bound_dict['repeat_distance'],
                    lower_bound_dict['repeat_broadening'],
                    lower_bound_dict['first_peak_offset'],
                    lower_bound_dict['amp_peak_1'],
                    lower_bound_dict['amp_peak_2'],
                    lower_bound_dict['amp_peak_3'],
                    lower_bound_dict['amp_peak_4'],
                    lower_bound_dict['bg_slope'],
                    lower_bound_dict['bg_offset']
                    ]

    upper_bounds = [upper_bound_dict['repeat_distance'],
                    upper_bound_dict['repeat_broadening'],
                    upper_bound_dict['first_peak_offset'],
                    upper_bound_dict['amp_peak_1'],
                    upper_bound_dict['amp_peak_2'],
                    upper_bound_dict['amp_peak_3'],
                    upper_bound_dict['amp_peak_4'],
                    upper_bound_dict['bg_slope'],
                    upper_bound_dict['bg_offset']
                    ]

    bounds = (lower_bounds, upper_bounds)

    model_with_fit_settings.initial_params = (
        initial_params
        )
    model_with_fit_settings.param_bounds = (
        bounds
        )
    model_with_fit_settings.vector_input_model = (
        linmods.lin_repeat_after_offset_4_vectorargs
        )

    return model_with_fit_settings


def set_up_model_linear_fit_with_fit_settings():
    """Set up the RPD model with fitting settings.
    The fitting settings are to pass to scipy's
    curve_fit, and the vector-input version of the model is for
    differentiation and error propagation with numdifftools.

    Args:
        None

    Returns:
        A ModelWithFitSettings object containing:
            model_rpd (function name):
                Relative position density as a function of separation
                between localisations.
            initial_params (list):
                Starting guesses for the parameter values by
                scipy.optimize.curve_fit
            lower_bounds (list), upper_bounds (list):
                The bounds on allowable parameter values as
                scipy.optimize.curve_fit runs.
    """
    # Generate ModelWithFitSettings object, conatining a model_rpd
    model_with_fit_settings = (
        ModelWithFitSettings(model_rpd=linmods.linear_fit)
        )

    # Add fitting parameters to ModelWithFitSettings object
    (lower_bound_dict,
     upper_bound_dict,
     initial_params_dict) = create_default_fitting_params_dicts()

    # Can optionally modify these dictionaries here:

    initial_params = [initial_params_dict['bg_slope'],
                      initial_params_dict['bg_offset']
                      ]

    lower_bounds = [lower_bound_dict['bg_slope'],
                    lower_bound_dict['bg_offset']
                    ]

    upper_bounds = [upper_bound_dict['bg_slope'],
                    upper_bound_dict['bg_offset']
                    ]

    bounds = (lower_bounds, upper_bounds)

    model_with_fit_settings.initial_params = (
        initial_params
        )
    model_with_fit_settings.param_bounds = (
        bounds
        )
    model_with_fit_settings.vector_input_model = (
        linmods.linear_fit_vector_args
        )

    return model_with_fit_settings
