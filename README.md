# PERPL (Pattern Extraction from Relative Positions of Localisations) 
This project provides functions for finding relative positions between points in 3D space and plotting as distance histograms for single molecule localisation data e.g. direct stochastic optical reconstruction microscopy (dSTORM) or photoactivated light microscopy (PALM).

The software uses a file containing localisation data, analyses the distribution of relative positions between them within a certain maximum distance ('filter distance') and outputs and saves these relative positions. The filter distance is applied in 3D (or in 2D, as required).

This algorithm was developed by **Alistair Curd** of the University of Leeds on 30 July 2018.


Copyright 2018 Peckham Lab


It was ported from Python 2 to Python 3 and made more user friendly by **Joanna Leng** at the University of Leeds who is funded by EPSRC as a Research Software Engineering Fellow (EP/R025819/1).

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

## DEVELOPED WITH: 
This was developed using Python 3.7.3 and Anaconda, Inc. on a Centos 7 and a Windows 10 system. The software was exectuted a limited number of times on an Apple Mac.

## QUICK START: 
Immediately below are a set of instructions that allow you to execute the PERPL analysis software quickly. There are no explanations of the steps here. Please look at the rest of the README file if you have any problems.

This software uses Anaconda with Python 3 so you will need to install and open an Anaconda shell. Once that is open, type the following the FIRST time you run the PERPL software (it is not required for later runs):

`conda env create -f environment_perpl.yml`

Next, activate the PERPL Anaconda environment using the following command:

`conda activate PERPL`

Execute the relative_positions.py Python script that analyses suitably processed experimental data. This should be localisation data that has been processed into x and y (2D) or x, y and z (3D) coordinates stored in a text or .csv file. You will be asked to provide input, such as the input data filename, to the script as it executes. Example data files are not stored with the software in a version control system as they are large, so use up quota, and and do not change, so do not require versioning. We plan to put the data into Zenodo and then include instructions here on how to access them.

`python relative_positions.py`

Execute the rot_2d_symm_fit.py Python script. This will read in output data from the relative_positions.py script and compare it to a model of localisations with 2D rotational symmetry. Again you will be asked to provide input, which should a list of relative positions, not a list of localisations.

`python rot_2d_symm_fit.py`


## ENVIRONMENT:
The Anaconda environment, with all the necessary modules, can be set up using the *environment_perpl.yml* file. 

To see what conda environments you have, run the command

`conda env list`

To create a new Anaconda environment for PERPL, run the command

`conda env create -f environment_perpl.yml`

To start using the environment, run the command

`conda activate PERPL`

To stop using that enviroment:

`conda deactivate`

To remove the environment, if you no longer want to use PERPL:

`conda remove --name PERPL --all` 

## USAGE:
There are two Python scripts that can be executed from the command line from a shell with Python 3 available, these are relative_positions.py and rot_2d_symm_fit.py.
Each of these can be run from the command line in a couple of ways:

1. They can be executed and all the necessary parameters for the code to execute successfully can be provided as flags/arguments at the command line. 
2. They can be executed from the command line where no flags/arguments are provided. In this case the user is asked to provide the necessary information as the script executes via a file browser for the input file and via the command line for all other information.

In both these cases the user can choose to execute the code in verbose mode and monitor its progress via output printed to the command line (standard output).

Each time these scripts run, a html report is created in the directory where the input data is stored. The paths and filenames provide information on when the script executed and the parameters selected, as well as being documented in the report. The image files used in the report are saved to the directory with the report. You can view the report in a web browser on the machine where it was created by double clicking on the html file in the directory. If you wish to share the file, you can print it (or save it as a pdf if correctly configured) via your web browser. If you wish to share the html report as html, remember to share the image files with the html file.

### relative_positions.py

To execute interactively, provide no flags (arguments) and type:

`python relative_positions.py`

To execute silently with default values, all you need is to include a data file; type:

`python relative_positions.py  -i data_file.csv`

To execute silently with your own values you also need to include a data file; type for example:

`python relative_positions.py  -i data_file.csv -d 2 -f 200 -z 12`

To get information on the flags and usage, type:

`python relative_positions.py  -h`

### rot_2d_symm_fit.py
This script is executed to compare a model with rotational 2D symmetry with experimental fluorescence localisation microscopy data. The script reads in output data generated by relative_positions.py and compares it to a model that it generates.

To execute interactively, provide no flags (arguments) and type:

`python rot_2d_symm_fit.py`

To execute silently with default values, all you need is to include a data file; type:

`python rot_2d_symm_fit.py  -i data_file_output_from_relative_positions.csv`

To execute silently with your own values, you need to include a data file and filter distance (the filter distance only has an effect if it is less than that used in generating the input data when executing relative_positions.py); type for example:

`python relative_positions.py  -i data_file_output_from_relative_positions.csv -f 100`

To get information on the flags and usage, type:

`python relative_positions.py  -h`

Examples of usage are in the bash script command_line_demo.sh.


### EXAMPLE RUN

Install Anaconda or Miniconda from anaconda.org. Use an environment with the required packages installed. 

To run from the command line and be prompted to input filename and settings, type:

`python relative_positions.py`

Read in file: Nup107_SNAP_3D_GRROUPED_10nmZprec_10000_from_36297_locs.csv

Choose 3D analysis

Set filter distance to 200 nm (this should run for under a minute)

To run from the command line providing the name of the input file, type:

`python relative_positions.py -i /usr/not-backed-up/AlistairCurd/repro/Nup107_SNAP_3D_GRROUPED_10nmZprec.txt -d 3 -f 200 -z 15 -v`

### DOXYGEN DOCUMENTATION
Doxygen documentation for this project can be created. Python doxygen is part of the PERPL environment so you do not need to install doxygen.

Run the command in the top directory of the source code:

`doxygen`

After this has run a doc/html directory will appear. Open the index.html file in this directory.

The doxygen documentation for this project lists gives the API (Application Programmers Interface) for all the modules and scripts in this project making it very useful to developers who wish to futher develop this software.

### DATA
The script command_line_demo.sh is a Linux shell script that executes the scripts relative_positions.py and rot_2d_symm_fit.py. To run this script create a directory called 'data' in the directory on your local system that holds this source code. Download the data, put it in that directory and then the script should run.

## FILES INCLUDED: 

* *README.md*: This file, which contains information about the software in this project.
* *command_line_demo.sh*: A bash script that executes the Python scripts relative_positions.py and rot_2d_symm_fit.py with various command line options.
* *Doxyfile*: Configuration file for the automated documentation system doxygen.
* *environment_perpl.yaml*: Anaconda configuration file that creates the environment used to develop this software.
* *license.md*: Software licensing file.
* *.gitignore*: Hidden, configuration file for git, a distributed version-control system.

### Python Code

* *relative_positions.py*:  A Python script which calculates the relative positions between points detected during fluorescence localisation microscopy.
* *rot_2d_symm_fit.py*:     A Python script which fits rotational symmetry models to relative positions among localisation microscopy data. The models are generated from synthetic localisation data.

* *background_models.py*:   A Python module which creates models of the background in fluorescence localisation microscopy data.
* *centriole_analysis.py*: A Python module which fits centriole model data to relative positions among localisation microscopy data. The models are generated from synthetic localisation data.
* *dna_paint_data_fitting.py*: A Python module which fits model data to relative positions among localisation microscopy data from DNA-PAINT imaging of a DNA-origami structure. The models are generated from synthetic localisation data, e.g. in *polyhedramodelling.py*.
* *linearrepeatmodels.py*: A Python module containing candidate models for relative position distributions in a 1D arrangement of localisations of a target protein.
* *modelling_general.py*: A Python module with functions generally useful for analysing relative positions, and generating and fitting models of fluorescence localisation microscopy data.
* *modelstats.py*: A Python module with statistical functions useful for analysing models against experimental data.
* *plotting.py*: A Python module with functions to create plots of data and analysis results.
* *polyhedramodelling.py* A Python module containing candidate models for relative position distributions in simple polyhedral arrangements of localisations of a target protein.
* *reports.py*: A Python module with functions to produce html reports for the python scripts relative_positions.py and rot_2d_symm_fit.py.
* *two_layer_fitting.py*: A Python module which fits a two-layer model of locallisation distribution to experimental data.
* *utils.py*: A Python module with useful functions.
* *zdisk_modelling.py*: A Python module which fits models of relative positions in Z-disk data to relative positions among localisation microscopy data.

* *test_relative_positions.py*: test file for the relative_positions.py module.
* *test_linearrepeatmodels.py*: test file for the linearrepeatmodels.py module.

Data is separate to this software repository.

### Unittests

There are unit tests in the tests directory. These will be of interest to a software engineer who wishes to extend this project. They can be run from a Python 3 shell with the command.

`python -m unittest discover -s tests`


## INFORMATION ON THIS PROJECT:
Python 3 version of PERPL analysis scripts.

