"""
plotting.py

Library to creates plots of the modelled data.

Created on Mon Apr 29 14:52:57 2019

Alistair Curd
University of Leeds
30 July 2018

Software Engineering practices applied

Joanna Leng (an EPSRC funded Research Software Engineering Fellow (EP/R025819/1)
University of Leeds
January 2019

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import sys
from sys import platform as _platform
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from scipy.ndimage.filters import gaussian_filter1d
import modelling_general as models
# information on backends is here
# https://matplotlib.org/3.1.1/tutorials/introductory/usage.html#backends
#if _platform == "linux" or _platform == "linux2":

   # linux
#   matplotlib.use('TkAgg')
#elif _platform == "darwin":
   # MAC OS X
#   matplotlib.use('MacOSX')
#elif _platform == "win32":
   # Windows
#   matplotlib.use('TkAgg')
#elif _platform == "win64":
    # Windows 64-bit
#    matplotlib.use('TkAgg')

if _platform == "darwin":
       # MAC OS X
    matplotlib.use('MacOSX')


def plot_estci(axes, mod, popt, perr, reps, fitlength, ci, color='xkcd:red'):
    """Creates a graph.

    Args:
       axes: Matplotlib plot.
       mod:
       popt:
       perr:
       reps:
       fitlength:
       ci:
       color:

    Returns:
       Nothing is returned
    """
    # Sample model parameters
    poptsamples = np.zeros((reps, len(popt)))
    modsamples = np.zeros((reps, fitlength))

    x_values = np.arange(fitlength) + 0.5

    for i in range(reps):
        for p in range(len(popt)):
            poptsamples[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
        modsamples[i] = mod(x_values, *poptsamples[i])
    modsort = np.sort(modsamples, axis=0)

    axes.plot(x_values, mod(x_values, *popt), color=color, lw=0.75)
    axes.fill_between(x_values,
                      modsort[int(round(len(modsort) / 100.
                                        * (100 - ci) / 2))],
                      modsort[int(round(len(modsort) / 100.
                                        * (ci + (100 - ci) / 2)))],
                      facecolor=color,
                      alpha=0.25)


def draw_2d_scatter_plots(xyz_values, dims, info, zoom):
    """Farms out the scatter plots that need to be plotted to the draw_2d_scatter_plot
    function.

    Args:
       xyz_values (numpy array): A numpy array of the localizations.
       dims (int): The dimensions of the data ie 2D or 3D.
       info (dict): A python dictionary containing a collection of useful parameters.
       zoom (int): A zoom of the central region can be created to this zoom factor.

    Returns:
       Nothing is returned.
    """
    if zoom == 0:
        title = "Scatter plot of localisations in XY"
        fig_name = r'scatter_plot_xy_localisations.png'
        filename = info['results_dir']+r'/'+fig_name
        draw_2d_scatter_plot(xyz_values[:, [0]], xyz_values[:, [1]],
                             title, filename, 'X (nm)', 'Y (nm)')

        if dims == 3:
            title = "Scatter plot of localisations in XZ"
            fig_name = r'scatter_plot_xz_localisations.png'
            filename = info['results_dir']+r'/'+fig_name
            draw_2d_scatter_plot(xyz_values[:, [0]], xyz_values[:, [2]],
                                 title, filename, 'X (nm)', 'Z (nm)')


            title = "Scatter plot of localisations in YZ"
            fig_name = r'scatter_plot_yz_localisations.png'
            filename = info['results_dir']+r'/'+fig_name
            draw_2d_scatter_plot(xyz_values[:, [1]], xyz_values[:, [2]],
                                 title, filename, 'Y (nm)', 'Z (nm)')

    if zoom > 0:
        x_values_masked = find_thresholds(xyz_values[:, 0], zoom)
        y_values_masked = find_thresholds(xyz_values[:, 1], zoom)
        #if dims == 3:
        #    z_values_masked = find_thresholds(xyz_values[:, 2], zoom)

        title = "Scatter plot of localisations in XY: zoom x{:d} on central region".format(zoom)
        fig_name = r'scatter_plot_xy_localisations_x'+str(zoom)+'.png'
        filename = info['results_dir']+r'/'+fig_name
        draw_2d_scatter_plot(x_values_masked, y_values_masked,
                             title, filename, 'X (nm)', 'Y (nm)')

        #if dims == 3:
            #title = "Scatter Plot of XZ Locations: Center with Zoom of x{:d}".format(zoom)
            #fig_name = r'scatter_plot_xz_locations_x'+str(zoom)+'.png'
            #filename = info['results_dir']+r'/'+fig_name
            #draw_2d_scatter_plot(x_values_masked, z_values_masked,
             #                    title, filename, 'x (nm)', 'z (nm)')

            #title = "Scatter Plot of YZ Locations: Center with Zoom of x{:d}".format(zoom)
            #fig_name = r'scatter_plot_yz_locations_x'+str(zoom)+'.png'
            #filename = info['results_dir']+r'/'+fig_name
            #draw_2d_scatter_plot(y_values_masked, z_values_masked,
            #                     title, filename, 'y (nm)', 'z (nm)')





def draw_2d_scatter_plot(x_values, y_values, title, filename, x_label, y_label):
    """Creates and scatter plot and saves it to a .png file.

    Args:
        x_values (numpy array): A numpy array of the co-ordinate of the
            localizations that will be plotted along the horizonal axis.
        y_values (numpy array): A numpy array of the co-ordinate of the
            localizations that will be plotted along the vertical axis.
        title (str): the text string that has the title for the plot.
        filename (str): The output file has all the text of the input
            file plus some more information so it is easy to recognise which
            it has been derived from.

    Returns:
       Nothing is returned.
    """

    fig = plt.figure(num=None, figsize=(10, 8), dpi=200, facecolor='w',
                     edgecolor='k')
    plt.scatter(x_values, y_values, s=1)
    plt.title(title)
    plt.xlabel(x_label, {'fontsize':'14'})
    plt.ylabel(y_label, {'fontsize':'14'})
    # options on scaleing are here
    # https://matplotlib.org/devdocs/api/_as_gen/matplotlib.axes.Axes.set_aspect.html
    plt.axis('scaled')
#    print(filename)
    fig.savefig(filename, bbox_inches='tight')
    #fig.show()



def find_thresholds(arr, zoom):
    """Calaculates the thresholds for the zoom into the scatter plots and applies
    them to mask of the input array.

    Args:
       arr (numpy array): A numpy array of one of the co-ordinates for the locationation.
       zoom (int): A zoom of the central region can be created to this zoom factor.

    Returns:
       arr (numpy.ma.core.MaskedArray): A numpy mask array of one of the
            thresholded co-ordinates for the locationation.
    """
    arr_min = np.min(arr)
    arr_max = np.max(arr)

    data_range = arr_max - arr_min
    zoom_bin = data_range / zoom

    if zoom%2 == 1:
        min_threshold = arr_min + (zoom_bin * int(zoom/2))
        max_threshold = arr_max - (zoom_bin * int(zoom/2))
    else:
        min_threshold = arr_min + (zoom_bin * (int(zoom/2)-0.5))
        max_threshold = arr_max - (zoom_bin * (int(zoom/2)-0.5))

    arr = np.ma.array(arr)
    arr_masked = \
        np.ma.masked_where(\
                (np.logical_xor((min_threshold < arr), (arr < max_threshold))), \
                arr, np.nan)

    return arr_masked


def plot_histograms(d_values, dims, filter_distance, info):
    """Farms out the histograms that need to be plotted to the plot_histogram
    function.

    Args:
       d_values (numpy array): A numpy array of the localizations and distance
            between localisations.
       dims (int): The dimensions of the data ie 2D or 3D.
       filterdist (int): The distance within which relative positions were calculated.
       info (dict): A python dictionary containing a collection of useful parameters
            such as the filenames and paths.

    Returns:
        Nothing is returned.

    """


    plot_histogram(np.absolute(d_values[:, 3]), "xy", filter_distance, info)
    #print("xy d_values[:, 3][0]: ", d_values[:, 3][0])

    plot_histogram(np.absolute(d_values[:, 0]), "x", filter_distance, info)
    #print("x d_values[:, 0][0]: ", d_values[:, 0][0])
    plot_histogram(np.absolute(d_values[:, 1]), "y", filter_distance, info)


    if dims == 3:
        plot_histogram(np.absolute(d_values[:, 4]), "xz", filter_distance, info)
        plot_histogram(np.absolute(d_values[:, 5]), "yz", filter_distance, info)
        plot_histogram(np.absolute(d_values[:, 6]), "xyz", filter_distance, info)

        plot_histogram(np.absolute(d_values[:, 2]), "z", filter_distance, info)



def plot_new_histogram(data_values):
    """Creates a histogram with a smooth curve fitted to it.

    Args:
       data_values (numpy array): A numpy array of the distance between localisations.

    Returns: No return value

    """

    # Unbinned axial relative positions

    fitlength = 100.

    # In this case my relative positions
    #ax_points = relpos.axial[(relpos.axial < fitlength) & (relpos.transverse < 10)]
    ax_points = data_values[(data_values[:0] < fitlength) & (data_values[:, 6] < 10)]

    # processing resulted in X distances (relpos.axial) and YZ distances (relpos.tranverse).

    # Sort and remove duplicates
    ax_points = np.sort(ax_points)
    ax_points = ax_points[::2]

    # Plot histogram
    axes = plt.hist(ax_points, bins=np.arange(fitlength + 1), color='xkcd:lightblue')[0]

    # Smooth and plot histogram
    # 4.4 here is np.sqrt(2) * localisation precision estimate
    line_smooth = gaussian_filter1d(axes, 4.4)

    # (which the user could input)
    plt.plot(np.arange(fitlength) + 0.5, line_smooth, color='xkcd:red')



def plot_histogram(data_values, data_description, filterdist, info):
    """Creates a histogram and saves it into the .png file.

    Args:
       data_values (numpy array): A numpy array of the distance between localisations.
       data_description (str): A description of the distance eg xy or xz that
            is added to the name of the .png file.
       filterdist (int): The distance within which relative positions were calculated.
       info (dict): A python dictionary containing a collection of useful parameters
            such as the filenames and paths.

    Returns:
       histogram_values (numpy array): A numpy array of the normalised probability
           density values for each bin.

    """

    data_max = data_values.max()

    if data_max < 5:
        start = 0.0
        end = math.ceil(data_max*10)/10
        edges = math.ceil(data_max*10) + 1
        bins = np.linspace(start, end, edges)
    else:
        start = 0.0
        end = filterdist
        edges = filterdist + 1
        bins = np.linspace(start, end, edges)


    fig_hist = plt.figure(num=None,
                          figsize=(10, 8),
                          dpi=100,
                          facecolor='w',
                          edgecolor='k')
    axes = fig_hist.add_subplot(111)

    fig_name = r'histogram_'+data_description.replace(" ", "_")+r'_separation_in_nm.png'
    filename = info['results_dir']+r'/'+fig_name


    title = r"Distance histogram of "+data_description.upper()+r" separations"

    plt.title(title)

    plt.xlabel(data_description.upper()+r' separation (nm)')
    plt.ylabel('Counts')

    bin_heights, bin_edges = np.histogram(data_values[data_values < filterdist], bins)

    axes.hist(data_values, bins, color='darkblue', edgecolor='k', linewidth=1,\
              alpha=0.5)


    fig_hist.savefig(filename, bbox_inches='tight')
    #fig_hist.show()


    histo_name = r'histogram_'+data_description.replace(" ", "_")+r'.csv'
    filename1 = info['results_dir']+r'/'+histo_name

    data_values = pd.concat([pd.DataFrame(bin_heights),
                             pd.DataFrame(bin_edges)], axis=1)

    head = "normalised probability density,bin edges"

    try:
        np.savetxt(filename1, data_values, delimiter=',', header=head, comments='')
    except (EOFError, IOError, OSError):
        print("Unexpected error:", sys.exc_info()[0])
        sys.exit("Could not create and open the output data file.")

    return bin_heights



def hist1d(nns, filterdist):
#    d_values[:, 3], "xy distances", results_dir, dims
    """Plots a histogram of Euclidean distances from relative positions.

    Args:
        nns: Array of 3D relative positions.
        filterdist: In getdistances, this was the distance within which
            relative positions were calculated. It is used here to set the
            maximum value for the histogram.

    Returns:
        H: histogram of Euclidean distances between localisations, from
           array of relative positions between them (calculated with
           getdistances, for instance). In 1 nm bins, ending at filterdist.
    """

    if nns.shape[1] == 2:
        nns = np.column_stack((nns, np.zeros(nns.shape[0])))

    histogram_values = np.histogram(np.sqrt(
        nns[:, 0] ** 2 + nns[:, 1] ** 2 + nns[:, 2] ** 2), bins=range(filterdist))[0].astype(float)
    plt.plot(histogram_values)

    return histogram_values




def plot_histogram_with_curves(bin_values, xy_histogram, symmetries, x_values, curve_values, info):
    ''' Plots a histogram of seprations with curves that show how well various models
    fit this histogram.
    Args:
        bin_values: An array of the histogram bin boundaries.
        xy_histogram: An array of floats giving the separations.
        symmetries: Array of the range of rotational symmerties the model tests.
        x_values: An array of the points on the x axesthat the curve are on the plot.
        curve_values: A 2d array of the curves for each symmertry.
        info (dict): A python dictionary containing a collection of useful
                     parameters such as the filenames and paths.

    Returns:
        nothing
    '''

    fig_histogram = plt.figure(num=None,
                               figsize=(10, 8),
                               dpi=100,
                               facecolor='w',
                               edgecolor='k')

    axes = fig_histogram.add_subplot(111)
    center = (bin_values[:-1] + bin_values[1:]) / 2
    width = 1.0
    axes.bar(center,
             xy_histogram,
             align='center',
             width=width,
             alpha=0.5,
             color='lightgrey')

    for i, symm in enumerate(symmetries):
        line_label = str(symm)+"-fold symmetry"
        axes.plot(x_values,
                  curve_values[i],
                  label=line_label)

    plt.title('5-fold to 11-fold fits',
              fontsize=14,
              color='black')
    plt.xlabel('XY separation (nm)', fontsize=14, color='black')
    plt.ylabel('Counts (normalised)', fontsize=14, color='black')

    plt.legend()
    filename = info['results_dir']+r'/'+r'Histogram_with_Fitted_Curves.png'
    fig_histogram.savefig(filename, bbox_inches='tight')
    #fig_histogram.show()



def plot_rot_2d_geometry(sym, diameter, info):
    ''' Plots the rotational geometry used in a model.
    Args:
        sym: Integer that represents the rotational symetry of the model.
        diamter: Integer that represents the diamter of the model
        info (dict): A python dictionary containing a collection of useful
                     parameters such as the filenames and paths.
    Returns:
        nothing

    '''

    #small_font = 6
    #medium_font = 8
    #plt.rc('axes', titlesize=medium_font)     # fontsize of the axes title
    #plt.rc('axes', labelsize=medium_font)    # fontsize of the x and y labels
    #plt.rc('xtick', labelsize=small_font)    # fontsize of the tick labels
    #plt.rc('ytick', labelsize=small_font)
    #plt.rc('legend', fontsize=small_font)    # legend fontsize
    #plt.rc('figure', titlesize=medium_font)  # fontsize of the figure title

    fig = plt.figure(num=None,
                     figsize=(1.5, 1.5),
                     dpi=200,
                     facecolor='w',
                     edgecolor='k')

    title = "Plot of geometry with\n"+str(sym)+"-fold rotational symmetry"

    fig_name = "GeometryPlotRotationqalSymmetry"+str(sym)+r"Fold.png"

    filename = info['results_dir']+r'/'+fig_name

    verts = models.generate_polygon_points(sym, diameter)

    x_values = verts[:, 0]
    y_values = verts[:, 1]

    area = np.pi*20

    plt.scatter(x_values, y_values, s=area, c='blue')
    for point in range(0, sym, 1):
        if point < sym-1:
            x_point = (x_values[point], x_values[point+1])
            y_point = (y_values[point], y_values[point+1])
        if point == sym-1:
            x_point = (x_values[point], x_values[0])
            y_point = (y_values[point], y_values[0])
        plt.plot(x_point, y_point, 'k-')
    plt.title(title)
    plt.xlabel('x (nm)')
    plt.ylabel('y (nm)')

    fig.savefig(filename, bbox_inches='tight')
    #fig.show()
